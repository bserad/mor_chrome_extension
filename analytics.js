/**
 * MOR 97.1 Chrome Extension ~ Copyright (c) 2013 Bryan CS, yanser25[at]gmail[dot]com
 * Released wihtout license.
 */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-42095479-1']);
_gaq.push(['_trackPageview']);

(function() {
	
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'https://ssl.google-analytics.com/ga.js', true);
	xhr.responseType = 'blob';
	xhr.onload = function(e) {
		var blob = new Blob([this.response], { type: 'text/javascript' });		
		
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = window.webkitURL.createObjectURL(blob);;
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
		
		
		
	};

	xhr.send();



  
})();